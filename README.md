# 基于51单片机的DS18B20测温系统

## 项目描述

DS18B20是一款数字温度传感器，广泛应用于温度测量领域。本项目将介绍基于51单片机的DS18B20测温系统设计。通过该系统，您可以实时读取环境温度，并在温度超过设定阈值时发出警报。

## 设计所需物品

- 51单片机开发板
- DS18B20数字温度传感器
- 4位LED数码管模块
- 蜂鸣器模块
- 其他相关组件（如电阻、电容等）

## 设计思路

1. **传感器连接**：将DS18B20数字温度传感器连接到51单片机开发板上，并读取传感器输出的数字温度值。
2. **温度显示**：通过LED数码管模块将温度值显示出来，同时加入单位°C。
3. **警报功能**：如果温度超过设定阈值，发出警报声。

## 设计步骤

1. **硬件连接**：
   - 按照DS18B20数字温度传感器的接口说明将其与开发板连接。DS18B20需要三个引脚：数据线、VCC和GND。
   - 将LED数码管模块和蜂鸣器模块按照其接口说明与开发板连接。

2. **程序编写**：
   - 编写读取DS18B20温度数据的程序，并将其烧录至单片机。
   - 编写LED数码管显示温度值的程序，并将其烧录至单片机。
   - 编写程序，将读取到的温度值经过转换后显示在LED数码管上，并在温度超过设定阈值时触发警报。

3. **程序结构**：
   - DS18B20温度传感器初始化和读取
   - LED数码管显示温度值
   - 警报触发

4. **系统测试**：
   - 将设计好的DS18B20测温系统连接到电源，进行功能测试。

## 注意事项

- 确保所有硬件连接正确无误。
- 在编写程序时，注意DS18B20的时序要求。
- 测试时，注意观察温度显示是否准确，警报功能是否正常触发。

## 资源文件

- `DS18B20_Temperature_Sensor.c`：读取DS18B20温度数据的程序。
- `LED_Display.c`：LED数码管显示温度值的程序。
- `Alarm_Trigger.c`：警报触发程序。
- `main.c`：主程序，整合以上功能。

## 贡献

欢迎大家提出改进建议或提交代码优化。如果您有任何问题或建议，请在Issues中提出。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。